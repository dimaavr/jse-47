package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;
import javax.xml.ws.WebServiceException;
import java.util.List;
import java.util.Random;

public class AdminUserEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private SessionDTO session;

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private final Random r = new Random();

    @BeforeClass
    public static void beforeClass() {
        if (!bootstrap.getUserEndpoint().existsUserByLogin("Test")) {
            bootstrap.getUserEndpoint().registryUser("Test", "Test", "Test@email.com");
        }
    }

    @Before
    public void before() {
        session = bootstrap.getSessionEndpoint().openSession("Admin", "Admin");
    }

    @After
    public void after() {
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllUser() {
        @NotNull final List<UserDTO> list = adminUserEndpoint.findAllUser(session);
        Assert.assertNotNull(list);
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserById() {
        @NotNull final UserDTO user = adminUserEndpoint.findUserById(session, session.getUserId());
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserByLogin() {
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "Admin");
        Assert.assertNotNull(user);
        Assert.assertEquals("Admin", user.getLogin());
    }

    @Test
    @Category(SoapCategory.class)
    public void lockUserByLogin() {
        adminUserEndpoint.lockUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockByLogin() {
        adminUserEndpoint.unlockUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserById() {
        @NotNull final int numberEmail = r.nextInt(100);
        @NotNull final UserDTO user = adminUserEndpoint.updateUserById(session, "FirstNameNew", "LastNameNew", "MiddleNameNew", "EmailNew" + numberEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew", user.getFirstName());
        Assert.assertEquals("LastNameNew", user.getLastName());
        Assert.assertEquals("MiddleNameNew", user.getMiddleName());
        Assert.assertEquals("EmailNew" + numberEmail, user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserByLogin() {
        @NotNull final int numberEmail = r.nextInt(100);
        @NotNull final UserDTO user = adminUserEndpoint.updateUserByLogin(session, "Admin", "FirstNameNew2", "LastNameNew2", "MiddleNameNew2", "EmailNewTest" + numberEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew2", user.getFirstName());
        Assert.assertEquals("LastNameNew2", user.getLastName());
        Assert.assertEquals("MiddleNameNew2", user.getMiddleName());
        Assert.assertEquals("EmailNewTest" + numberEmail, user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void setUserRole() {
        @NotNull final UserDTO userTest = adminUserEndpoint.findUserByLogin(session, "User");
        @NotNull final UserDTO user = adminUserEndpoint.setUserRole(session, userTest.getId(), Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.setUserRole(session, userTest.getId(), Role.USER);
    }

}