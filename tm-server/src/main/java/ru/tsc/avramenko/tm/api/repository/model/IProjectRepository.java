package ru.tsc.avramenko.tm.api.repository.model;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project findById(final String userId, final String id);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    void removeById(final String userId, final String id);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    void clear(final String userId);

    List<Project> findAllById(final String userId);

}